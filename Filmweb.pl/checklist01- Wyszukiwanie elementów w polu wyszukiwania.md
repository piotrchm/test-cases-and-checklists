|Sprawdzenie|Oczekiwany rezultat
|-----|-------|
|1. Wprowadzenie 2 znaków|Wyświetlenie listy wyników wyszukiwania dla podanej frazy
|2. Czy wyszukanie, bez wprowadzenia żadnej wartości w pole, powoduje wyświetlenie wyników wyszukiwania?|Nie
|3. Czy kliknięcie ikony wyszukiwania powoduje wyświetlenie wyników wyszukiwania?|Tak
|4. Wprowadzenie 1 znaku|Komunikat: *Fraza za krótka*
|5. Wprowadzenie >2 znaki|Wyświetlenie listy wyników wyszukiwania dla podanej frazy
|6. Wprowadzenie 2 białych znaków|Użytkownik zostaje przekierowany do zakładki *BAZA FILMWEBU*
|7. Wprowadzenie 9000 znaków (sprawdzenie granicy technologicznej)|Komunikat: *Serwis chwilowo niedostępny. Przepraszamy.*
|8. Wprowadzenie słowa kluczowego w języku polskim (np. *podróż*)|Wyświetlenie listy wyników, gdzie wyszukiwany element zawiera wyszukiwane słowo
|9. Wprowadzenie słowa kluczowego w języku obcym (np. *wall*)|Wyświetlenie listy wyników, gdzie wyszukiwany element zawiera wyszukiwane słowo
|10. Czy po wprowadzeniu frazy, rozwija się lista sugerowanych wyników|Tak
|11. Czy w trakcie wprowadzania kolejnych znaków frazy, aktualizuje się lista sugerowanych wyników?|Tak
|12. Czy użytkownik może wkleić frazę do pola?|Tak
|13. Wprowadzenie błędnej nazwy szukanego elementu|Komunikat: *Niestety, nie znaleźliśmy wyników dla frazy (wprowadzona fraza), Sprawdź, czy nie masz literówki*<br> Wyświetlenie listy elementów: *NAJLEPIEJ DOPASOWANE WYNIKI*
|14. Wprowadzenie frazy ze znakiem specjalnym|Wyświetlenie listy wyników wyszukiwania dla podanej frazy
|15. Wprowadzenie ciągu znaków specjalnych|Komunikat: *Niestety, nie znaleźliśmy wyników dla frazy (wprowadzone znaki), Sprawdź, czy nie masz literówki*
|16. Wprowadzenie obcojęzycznej nazwy szukanego elementu, (nazwa dostępna w sekcji *inne tytuły* na stronie szczegółowej szukanego elementu)|Wyświetlenie listy wyników, z nazwami w języku, w którym użytkownik wprowadził frazę

