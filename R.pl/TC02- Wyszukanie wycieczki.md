**Cel testu**

Sprawdzenie poprawności działania podstawowej ścieżki funkcji wyszukiwania wycieczki

**Warunki początkowe**

brak

**Kroki:**

|Opis operacji|Spodziewane rezultaty
|------|------|
|1. Otwórz stronę [https://r.pl](https://r.pl)|Strona została otwarta|
|2. W wyszukiwarce wycieczek kliknij *Miejsce Pobytu*|Otwarcie okna z wyborem lokalizacji<br><br>Wyświetlone jest pole *Wyszukaj kraj lub region*<br><br>Wyświetlona jest lista dostępnych krajów i regionów<br><br>Użytkownik może rozwinąć wszystkie regiony poprzez *ROZWIŃ REGIONY* oraz zwinąć wszystkie (dostępne) rozwinietę regiony poprzez *ZWIŃ REGIONY*<br><br>Użytkownik może zaznaczyć wszystkie dostępne kraje i regiony poprzez *ZAZNACZ WSZYSTKIE* oraz zwinąć rozwinięte wszystkie (dostępne) kraje i regiony poprzez *WYCZYŚĆ ZAZNACZONE*|
|3. Wybierz lokalizację (np. Stany Zjednoczone)|Lokalizacja została wybrana|
|4. Kliknij *Ok, Gotowe*|Okno wyboru lokalizacji zostało zamknięte|
|5. Kliknij *Wybierz termin*|Otwarte okno kalendarza aktualnego roku<br><br>Miesiące, które minęły, mają szare tło i nie można ich zaznaczyć<br><br>Aktualny miesiąc i przyszłe miesiące są aktywne i można je zaznaczyć<br><br>Przeszłe daty są kolory szarego i nie można ich wybrać<br><br>Użytkownik może wybrać 1 z 3 opcji wyświetlania dat: *CAŁY MIESIĄC*, *PRZEDZIAŁ*, *OD DNIA*|
|6. Wybierz dostępny termin|Termin został wybrany<br><br>Okno z wyborem terminu zostało zamknięte<br><br>Wybrany termin jest wyświetlony w *Kiedy?*|
|7. Kliknij *Miejsce wyjazdu*|Otwarcie okna z wyborem miasta wyjazdu<br><br>Wyświetlane jest pole *Wyszukaj miasto*<br><br>Wyświetlana jest lista miast wyjazdu<br><br>Użytkownik może wybrać wszystkie miasta poprzez *ZAZNACZ WSZYSTKIE*|
|8. Wybierz miasto|Miasto zostało wybrane<br><br>Wybrane miasto zostało wyświetlone w polu *Wyszukaj miasto*|
|9. Kliknij *OK, GOTOWE*|Okno wyboru miasta zostało zamknięte<br><br>Wybrane miasto jest wyświetlane w sekcji *Skąd wyjazd?*|
|10. Kliknij pole z ikoną osoby|Zostało otwarte okno z wyborem liczby uczestników dorosłych i dzieci oraz liczby pokoi|
|11. Wybierz 2 osoby dorosłe i 1 pokój|Liczba uczestników i pokoi została wybrana
|12. Kliknij *SZUKAJ*|Wyświetlenie listy dostępnych wycieczek<br><br>W przypadku braku możliwości spełnienia wybranego kryterium zostaje wyświetlona lista wycieczek do wybranego miejsca z innymi kryteriami oraz  widnieje komunikat: *Nie znaleźliśmy odpowiadających ofert (niespełnione kryterium). Poniżej prezentujemy wycieczki, które mimo to mogą Cię zainteresować*|





















