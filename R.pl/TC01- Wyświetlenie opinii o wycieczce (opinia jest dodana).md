**Cel testu**

Sprawdzenie poprawnego wyświetlania opinii o wycieczce

**Warunki początkowe**

brak

**Kroki:**

|Opis operacji|Spodziewane rezultaty
|------|------|
|1. Otwórz stronę [https://r.pl](https://r.pl)|Strona została otwarta|
|2. W wyszukiwarce wycieczek kliknij *Szukaj*|Wyświetlenie listy dostępnych wycieczek|
|3. Kliknij wycieczkę, która ma dodaną co najmniej 1 opinię (na zdjęciu oferty znajduje się średnia ocena i liczba opinii)|Została otwarta strona szczegółowa oferty<br><br>Użytkownik może przejść do opinii na kilka sposobów:<br><br>- kliknięcie ""sprawdź opinie"" (przy ocenie ogólnej wycieczki)<br><br>- przejść do dołu strony, do sekcji *Opinie klientów*<br><br>- przewinąć stronę dół, z menu bocznego strony kliknąć *OPINIE*|
|4. Przejdź do sekcji opinii (patrz Oczekiwany rezultat krok 3)|Wyświetlana jest sekcja opinii<br><br>W sekcji znajdują się filtry opinii<br><br>Opinie można filtrować ze względu na typ turysty, najlepsze oceny oraz najnowsze opinie<br><br>Można wyświetlić listę wszystkich opinii poprzez *POKAŻ WSZYSTKIE OPINIE* (na dole sekcji *Opinie klientów*)|
|5. Kliknij wybraną opinię|Otwarte okno z wybraną opinią<br><br>Wyświetlana jest nazwa autora opinii, jego termin pobytu na ocenianej wycieczce<br><br>Wyświetlane są oceny autora za poszczególne aspekty wycieczki<br><br>Wyświetlana jest ilość osób oceniających opinię za przydatną ze wszystkich oceniających<br><br>Wyświetlana jest ogólna ocena wycieczki<br><br>Opinia może zawierać zdjęcia<br>Zdjęcia można przewijać i powiększać<br>Powiększone zdjęcie można zamknąć poprzez *X*<br><br>Wyświetlana jest treść opinii<br>Treść opinii podzielona jest na sekcje<br><br>Wyświetlana jest opcja oceny przydatności opinii<br>Można wybrać czy opinia była przydatna, czy nie<br><br>Okno opinii można zamknąć przez *X* lub kliknięcie poza polem opinii|










