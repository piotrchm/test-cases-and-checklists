|Sprawdzenie|Oczekiwany rezultat
|-----|-------|
|1. Czy kliknięcie przycisku zmiany hasła powoduje wyświetlenie dodatkowych dwóch pól:<br>*Nowe hasło*<br>*Powtórz nowe hasło*|Tak
|2. Kliknięcie przycisku ukrycia hasła w polu *Aktualne hasło*/*Nowe hasło*/*Powtórz nowe hasło*|Wprowadzona wartość jest widoczna w postaci tekstowej dla użytkownika
|3. Czy ponowne kliknięcie przycisku ukrycia hasła powoduje wyświetlenie wprowadzonej wartości w postaci kropek?|Tak
|4. Czy wciśnięcie przycisku *Tab* powoduje przejście do kolejnego pola?|Tak
|5. Czy wprowadzone wartości widoczne dla użytkownika jako kropki, użytkownik może skopiować?|Nie
|6. Czy wprowadzenie poprawnych wartości do pól i kliknięcie *Zapisz* powoduje zapisanie nowego hasła? |Tak
|7. Czy wprowadzenie niepoprawnych wartości do pól i kliknięcie *Zapisz* powoduje pojawienie się komunikatu o błędach?|Tak
|8. Wylogowanie użytkownika i zalogowanie z użyciem nowego hasła|Użytkownik został zalogowany do serwisu
|9. Wylogowanie użytkownika i zalogowanie z użyciem starego hasła|Błąd- *błędny e-mail lub hasło*
|10. Wprowadzenie 49390 znaków we wszystkie pola (sprawdzenia granicy technologicznej)|Błąd- *WYSTĄPIŁ BŁĄD<br>Nie udało się zapisać zmian. Odśwież stronę i spróbuj ponownie.*
|**Sprawdzanie pola *Aktualne hasło***|
|11. Wprowadzenie hasła aktualnie przypisanego do konta użytkownika|Pomyślna zmiana hasła
|12. Puste pole- niewprowadzona wartość|Błąd- *Wypełnij to pole.*
|13. Puste pole- brak wprowadzonej wartości w polu *Aktualne hasło* i wprowadzenie wartości w pozostałe pola|Pole *Aktualne hasło*- błąd: *Wypełnij to pole.*
|14. Inny ciąg znaków niż aktualne hasło|Błąd- *Niepoprawne aktualne hasło*
|**Sprawdzanie pola *Nowe hasło*- pole *Aktualne hasło* jest uzupełnione prawidłowym hasłem**|
|15. Wprowadzenie hasła aktualnie przypisanego do konta użytkownika|Pomyślna zmiana hasła
|16. Wprowadzenie 5 znaków|Błąd-*Hasło za krótkie, musi mieć co najmniej 6 znaków*
|17. Wprowadzenie 6 znaków|Pomyślna zmiana hasła
|18. Wprowadzenie >6 znaków|Pomyślna zmiana hasła
|19. Wprowadzenie 255 znaków|Pomyślna zmiana hasła
|20. Wprowadzenie 256 znaków|Błąd- *Hasło za długie, może mieć maksymalnie 255 znaków.*
|21. Wprowadzenie tylko znaku spacji|Błąd- *Musisz podać hasło*
|22. Puste pole (przed przejściem do pola)|Błąd- *Pole hasło jest wymagane*
|23. Puste pole- niewprowadzona wartość (gdy użytkownik przeszedł do pola)|Błąd- *Pole hasło jest wymagane*<br>Błąd- *Wypełnij to pole.*
|**Sprawdzanie pola *Powtórz nowe hasło*- pole *Aktualne hasło* jest uzupełnione prawidłowym hasłem**|
|24. Wprowadzenie poprawnego hasła takiego jak w polu *Nowe hasło*|Pomyślna zmiana hasła
|25. Wprowadzenie wartości innej niż wartość w polu *Nowe hasło*|Błąd- *Podane hasła nie są takie same*
|26. Puste pole- niewprowadzona wartość (przed przejściem do pola), gdy pole *Nowe hasło* zostało uzupełnione|Błąd- *Podane hasła nie są takie same*
|27. Puste pole- niewprowadzona wartość (po przejściu do pola), gdy pole *Nowe hasło* zostało uzupełnione|Błąd- *Wypełnij to pole.*
|28. Wprowadzenie wartości, gdy pole *Nowe hasło* jest nieuzupełnione|Błąd- *Pole hasło jest wymagane*




