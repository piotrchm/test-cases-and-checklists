**Cel testu**

Sprawdzenie działania funkcji połączenia konta Filmweb z kontem Facebook

**Warunki początkowe**

Użytkownik posiada aktywne konto w serwisie Filmweb

Użytkownik posiada aktywne konto w serwisie Facebook

Konto użytkownika Filmweb nie jest połączone z kontem w Facebook

**Kroki:**

1. Otwórz stronę [https://www.filmweb.pl](https://www.filmweb.pl)
2. Zaloguj się na konto użytkownika (login: test147 | hasło: admin1)
3. Kliknij  strzałkę rozwijanego menu obok nazwy konta
4. Kliknij *USTAWIENIA*
5. Najedź kursorem na *INNE*
6. Kliknij *POWIĄZANE KONTA*
7. Kliknij *Połącz z kontem Facebook*
8. Zaloguj się do konta Facebook (login: test | hasło: test)


**Spodziewane rezultaty**

Wyświetlenie komunikatu: *Twoje konto jest połączone z kontem Facebook.*


Zmiana nazwy przycisku: *Połącz z kontem Facebook" na "Rozłącz z kontem Facebook.*



