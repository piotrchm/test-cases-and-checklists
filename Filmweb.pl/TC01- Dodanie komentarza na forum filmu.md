**Cel testu**

Sprawdzenie, czy zalogowany użytkownik może dodać komentarz na forum Filmweb

**Warunki początkowe**

Użytkownik posiada aktywne konto w serwisie

**Kroki:**

1. Otwórz stronę https://www.filmweb.pl
2. Zaloguj się do konta użytkownika (login: test147 | hasło: admin1)
3. Kliknij *FILMY*
4. Wejdź na stronę szczegółową dowolnego filmu
5. Przewiń stronę w dół do *Forum*
6. Kliknij pierwszy temat na forum
7. Kliknij *Odpowiedz*
8. Wypełnij pole *Dodaj komentarz* (patrz Spodziewane rezultaty)
9. Kliknij ikonę dodania komentarza

**Spodziewane rezultaty**

| Wprowadzona wartość | Oczekiwany wynik |
| ---     | ---   |
|[pozostawione puste pole]  | **Błąd**- *Wypełnij to pole*, komentarz nie został dodany |
|<3 znaki | **Błąd**- *Wystąpił Błąd. Za krótki komentarz* |
| >3 znaki | komentarz został dodany |


