**Cel testu**

Sprawdzenie, czy zalogowany użytkownik może ocenić film

**Warunki początkowe**

Utworzone i aktywowane konto użytkownika

**Kroki:**

|Opis operacji|Spodziewane rezultaty|
|----------|-----------|
|1. Otwórz stronę<br> [https://www.filmweb.pl](https://www.filmweb.pl)|Otworzyła się strona główna|
|2. Kliknij *Zaloguj się*|Otworzył się panel z wyborem sposobu logowania|
|3. Kliknij *Zaloguj się kontem Filmweb*|Otworzył się panel logowania
|4. Wprowadź dane logowania<br> (login: test147/ hasło: admin1)|Dane zostały uzupełnione
|5. Kliknij *Zaloguj się*| Użytkownik został zalogowany<br><br>Użytkownik został przeniesiony na stronę główną
|6. Kliknij pole wyszukiwania|Pole wyszukiwania jest aktywne<br><br>W polu wyszukiwania miga kursor<br><br>Z pola wyszukiwania została rozwinięta lista najczęściej wyszukiwanych pozycji w serwisie
|7. W pole wyszukiwania wpisz *Shrek*|Dane zostały uzupełnione<br><br>Z pola wyszukiwania została rozwinięta lista proponowanych wyników
|8. Kliknij ikonę lupy|Użytkownik został przeniesiony do strony z wynikami wyszukiwania dla wpisanej frazy
|9. Kliknij  wyszukany wynik *Shrek*|Użytkownik został przeniesiony na stronę wybranej pozycji
|10. Kliknij dowolną gwiazdkę|Film został oceniony<br><br>Wyświetlona została ocena słowna<br><br>Wyświetlona została informacja kiedy film został oceniony





